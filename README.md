# bfn-test

This project uses typescript. As the packages `grenache-nodejs-X` do not have typings, a type definition is created to integrate with typescript.

## requirements

- npm and node (16)
- Execute `npm install` to install the dependencies

## usage

- Start gnache nodes
  - Execute `npm run grape:1` and `npm run grape:2`
- Start server
  - Execute `npm run server`
- Start client
  - Execute `npm run client`

## NOTES

- this projects do not consider users or balance, it only considers orders.
- as every client has a copy of the orderbook, each client can take orders, so it will overlap other client orders if they are not handle properly on the server as they could hold an outdated copy of the orderbook
- as client and server uses a http connexion, the messages are send from client to server and return the response what limit the live updates (as i saw, there is another package https://github.com/bitfinexcom/grenache-nodejs-ws) but it is not properly documented how to create a live connection and the instructions mention to use the http package. Because of this http connection, the client will pull every 10 seconds the orderbook from the server.
- as crypto use decimals, in order to avoid decimal precision errors (we could use some libraries that handle big numbers) we use integers and it should be the clients that render the amount with decimals if desired.
- the matching engine is shared by the client and servers, but unless we do a routing to fetch the orderbook for a given pair, every server needs to hold a copy of each orderbook, this is going to be a proble as it grows and adds new pairs. This would not be a problem for the client as client only connect to a given pair, it also could instantiate other pairs, but client it is easy to handle as it knows to which clients to connect
- as each client it holds its own copy, we could have a unique key that indicates if the orderbook it is updated before we commit the order to the orderbook and other clients
