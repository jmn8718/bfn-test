import { PeerRPCServer } from 'grenache-nodejs-http';
import Link from 'grenache-nodejs-link';
import { PORT, SERVER_HOST } from './config';
import { Engine } from './matching_engine';
import {
  EventRequestPayload,
  Events,
  IHandler,
  IOrderRequest,
  Pairs,
} from './types';

const link = new Link({
  grape: SERVER_HOST,
});
link.start();

const peer = new PeerRPCServer(link, {
  timeout: 300000,
});
peer.init();

const service = peer.transport('server');
service.listen(PORT);

setInterval(function () {
  link.announce(Events.OrderNew, service.port, {});
  link.announce(Events.Orderbook, service.port, {});
}, 1000);

const btcEurEngine = new Engine({
  pair: Pairs.BTC_EUR,
  asks: [
    [205, 45],
    [207, 35],
  ],
  bids: [
    [204, 12],
    [203, 55],
    [201, 30],
  ],
});

service.on(
  'request',
  (
    _rid: string,
    event: Events,
    payload: EventRequestPayload,
    handler: IHandler,
  ) => {
    if (payload.pair !== Pairs.BTC_EUR) {
      handler.reply(new Error(`Invalid pair ${payload.pair}`));
    }
    try {
      if (event === Events.Orderbook) {
        handler.reply(undefined, btcEurEngine.getOrderbook());
      } else if (event === Events.OrderNew) {
        handler.reply(
          undefined,
          btcEurEngine.processOrder(payload as IOrderRequest),
        );
      } else {
        throw new Error(`Invalid event ${event}`);
      }
    } catch (err: any) {
      handler.reply(err);
    }
  },
);
