import { PeerRPCClient } from 'grenache-nodejs-http';
import Link from 'grenache-nodejs-link';
import { SERVER_HOST } from './config';
import { Engine } from './matching_engine';
import {
  IOrderbookRequest,
  IOrderRequest,
  Events,
  IOrderbookResponse,
  Pairs,
  Side,
} from './types';

const link = new Link({
  grape: SERVER_HOST,
});
link.start();

const peer = new PeerRPCClient(link, {});
peer.init();

const placeOrder = (engine: Engine, order: IOrderRequest) => {
  try {
    const { success } = engine.processOrder(order);
    if (success) {
      peer.request(Events.OrderNew, order, { timeout: 10000 });
    }
  } catch (err) {
    console.error(err);
  }
};

const data = [
  { side: Side.SELL, quantity: 10, price: 205 },
  { side: Side.BUY, quantity: 10, price: 205 },
  { side: Side.BUY, quantity: 10, price: 202 },
  { side: Side.SELL, quantity: 100, price: 210 },
  { side: Side.BUY, quantity: 1, price: 209 },
  { side: Side.SELL, quantity: 102, price: 212 },
];

// eslint-disable-next-line @typescript-eslint/no-unused-vars
const placeOrders = (engine: Engine) => {
  data.forEach(item => {
    console.log('--------', item);
    const orderData: IOrderRequest = {
      pair: Pairs.BTC_EUR,
      ...item,
    };
    placeOrder(engine, orderData);
  });
};

const ORDERBOOK_REQUEST: IOrderbookRequest = {
  pair: Pairs.BTC_EUR,
};

let engine: Engine;

setInterval(() => {
  peer.request(
    Events.Orderbook,
    ORDERBOOK_REQUEST,
    { timeout: 10000 },
    (err?: Error, payload?: IOrderbookResponse) => {
      if (err) {
        console.error(err);
        process.exit(-1);
      }
      if (!payload) {
        process.exit(0);
      }
      if (engine) {
        engine.setOrderbook({
          bids: payload.bids,
          asks: payload.asks,
        });
      } else {
        engine = new Engine({
          pair: payload.pair,
          bids: payload.bids,
          asks: payload.asks,
        });
      }
    },
  );
}, 10000);

setInterval(() => {
  if (engine) {
    const increment = Date.now() % 10;
    const orderData: IOrderRequest = {
      pair: Pairs.BTC_EUR,
      side: Date.now() % 2 ? Side.SELL : Side.BUY,
      quantity: 1 + ((increment * Date.now()) % 30),
      price: 205 + (Date.now() % 2 ? -1 : 1) * increment,
    };
    placeOrder(engine, orderData);
  }
}, 5000);
