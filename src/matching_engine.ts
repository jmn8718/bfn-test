import {
  IOrderbookResponse,
  IOrderRequest,
  IOrderResponse,
  Pairs,
  Side,
} from './types';

export class Engine {
  private readonly _pair: Pairs;
  private _bids: [number, number][];
  private _asks: [number, number][];
  constructor({
    pair,
    bids = [],
    asks = [],
  }: {
    pair: Pairs;
    bids?: [number, number][];
    asks?: [number, number][];
  }) {
    this._pair = pair;
    this._bids = bids;
    this._asks = asks;
  }

  private _sell({ price, quantity }: { price: number; quantity: number }) {
    let quantityLeft = quantity;
    const firstBid = this._bids[0];
    if (firstBid && firstBid[0] >= price) {
      // match sell
      let bidIndex = 0;
      let takeBids = -1;
      while (
        bidIndex < this._bids.length &&
        price <= this._bids[bidIndex][0] &&
        quantityLeft > 0
      ) {
        if (this._bids[bidIndex][1] > quantityLeft) {
          // only taking a portion
          this._bids[bidIndex][1] -= quantityLeft;
          quantityLeft = 0;
        } else {
          // we are taking all
          quantityLeft -= this._bids[bidIndex][1];
          this._bids[bidIndex][1];
          takeBids = bidIndex;
        }
        if (quantityLeft > 0) {
          bidIndex++;
        }
      }
      if (takeBids > -1) {
        this._bids = this._bids.slice(takeBids + 1);
      }
    }

    if (quantityLeft > 0) {
      // add to bid
      const askIndex = this._asks.findIndex(([askPrice]) => price <= askPrice);
      if (askIndex > -1) {
        if (this._asks[askIndex][0] === price) {
          // increase quantity
          this._asks[askIndex][1] = this._asks[askIndex][1] + quantityLeft;
        } else if (askIndex === 0) {
          this._asks = [[price, quantityLeft] as [number, number]].concat(
            this._asks,
          );
        } else if (askIndex === this._asks.length - 1) {
          this._asks = [[price, quantityLeft] as [number, number]].concat(
            this._asks,
          );
          this._asks = [
            ...this._asks.slice(0, askIndex),
            [price, quantityLeft] as [number, number],
            this._asks[askIndex],
          ];
        } else {
          this._asks = [
            ...this._asks.slice(0, askIndex),
            [price, quantityLeft] as [number, number],
            ...this._asks.slice(askIndex + 1),
          ];
        }
      } else {
        this._asks = this._asks.concat([
          [price, quantityLeft] as [number, number],
        ]);
      }
    }
  }

  private _buy({ price, quantity }: { price: number; quantity: number }) {
    let quantityLeft = quantity;
    const firstAsk = this._asks[0];
    if (firstAsk && firstAsk[0] <= price) {
      // match buy
      let askIndex = 0;
      let takeAsks = -1;
      while (
        askIndex < this._asks.length &&
        price >= this._asks[askIndex][0] &&
        quantityLeft > 0
      ) {
        if (this._asks[askIndex][1] > quantityLeft) {
          // only taking a portion
          this._asks[askIndex][1] -= quantityLeft;
          quantityLeft = 0;
        } else {
          // we are taking all
          quantityLeft -= this._asks[askIndex][1];
          this._asks[askIndex][1];
          takeAsks = askIndex;
        }
        if (quantityLeft > 0) {
          askIndex++;
        }
      }
      if (takeAsks > -1) {
        this._asks = this._asks.slice(takeAsks + 1);
      }
    }
    if (quantityLeft > 0) {
      // add to bid
      const bidIndex = this._bids.findIndex(([bidPrice]) => price >= bidPrice);
      if (bidIndex > -1) {
        if (this._bids[bidIndex][0] === price) {
          // increase quantity
          this._bids[bidIndex][1] = this._bids[bidIndex][1] + quantityLeft;
        } else if (bidIndex === 0) {
          this._bids = [[price, quantityLeft] as [number, number]].concat(
            this._bids,
          );
        } else if (bidIndex === this._bids.length - 1) {
          this._bids = [
            ...this._bids.slice(0, bidIndex),
            [price, quantityLeft] as [number, number],
            this._bids[bidIndex],
          ];
        } else {
          this._bids = [
            ...this._bids.slice(0, bidIndex),
            [price, quantityLeft] as [number, number],
            ...this._bids.slice(bidIndex + 1),
          ];
        }
      } else {
        this._bids = this._bids.concat([
          [price, quantityLeft] as [number, number],
        ]);
      }
    }
  }

  private _processOrder({
    side,
    price,
    quantity,
  }: {
    side: Side;
    price: number;
    quantity: number;
  }): boolean {
    if (side === Side.BUY) {
      this._buy({ price, quantity });
    } else if (side === Side.SELL) {
      this._sell({ price, quantity });
    } else {
      return false;
    }
    return true;
  }

  public getOrderbook(): IOrderbookResponse {
    return {
      pair: this._pair,
      bids: this._bids,
      asks: this._asks,
    };
  }

  public setOrderbook({
    asks,
    bids,
  }: {
    bids: [number, number][];
    asks: [number, number][];
  }): void {
    this._asks = asks;
    this._bids = bids;
  }

  public processOrder({
    side,
    price,
    quantity,
  }: IOrderRequest): IOrderResponse {
    let success = false;
    if (quantity > 0) {
      success = this._processOrder({ side, price, quantity });
    }
    if (success) {
      console.log(`orderbook after ${side}`, {
        asks: this._asks,
        bids: this._bids,
      });
    }
    return {
      success,
    };
  }
}
