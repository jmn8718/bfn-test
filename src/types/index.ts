export enum Pairs {
  BTC_EUR = 'BTC/EUR',
  ETH_EUR = 'ETH/EUR',
  ETH_BTC = 'ETH/BTC',
}

export enum Side {
  BUY = 'buy',
  SELL = 'sell',
}

export interface IOrderbookRequest {
  pair: Pairs;
}

export interface IOrderRequest {
  pair: Pairs;
  side: Side;
  price: number;
  quantity: number;
}
export enum Events {
  OrderNew = 'order_new',
  Orderbook = 'orderbook',
}

export interface IOrderResponse {
  success: boolean;
}

export interface IOrderbookResponse {
  pair: Pairs;
  bids: [number, number][];
  asks: [number, number][];
}

export type EventRequestPayload = IOrderRequest | IOrderbookRequest;
export type EventResponsePayload = IOrderResponse | IOrderbookResponse;

export interface IHandler {
  reply: (err?: Error, payload?: EventResponsePayload) => unknown;
}
